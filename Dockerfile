FROM node:14

WORKDIR /usr/src/app

COPY src/package*.json ./

RUN npm install

COPY src/ .

ENV APPLICATION_INSTANCE=example

EXPOSE 8080

CMD [ "node", "count-server.js" ]
