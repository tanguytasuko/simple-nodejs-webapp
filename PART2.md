# Microservices architecture and cloud deployment

## Part 2

### Q1:  Define what is a Microservices architecture and how does it differ from a Monolithic application.
A microservice architecture allows you to divide a large application into small, independent services, each managing a specific function, whereas with a monolithic application all the application's functions are integrated into a single, large system

### Q2: Compare Microservices and Monolithic architectures by listing their respective pros and cons.
Pros of Microservices architectures : Improve data security, team optimization, attractive for engineers, ease of understanding, smaller and faster deployment
Cons of Microservices architectures : Upfront costs, communication between services is complex, more services equal more ressources
Pros of Monolotihic architecture : Easy to understand and develop, few communications problem
Cons of Monolithic architecture : difficult to modify and update

### Q3: In a Micoservices architecture, explain how should the application be split and why.
Split into different fonction so each part is easier to manage and develop

### Q4:  Briefly explain the CAP theorem and its relevance to distributed systems and Microservices.
The CAP theorem applies a similar type of logic to distributed systems—namely, that a distributed system can deliver only two of three desired characteristics: consistency, availability, and partition tolerance.
It's important to known for choosing how to manage data in microservices

### Q5: What consequences on the architecture ?
It requires choices between data consistency, availability and fault tolerence

### Q6: Provide an example of how microservices can enhance scalability in a cloud environment.
For example, in online website where they sell some clothes for example, if the number of orders increase then the microservice that manages orders can be extended without affecting other services

### Q7: What is statelessness and why is it important in microservices archi-tecture ?
"Statelessness" means that a microservice has no memory of previous interactions. Each request is treated as a new one. This feature is important because it makes easier the evolution and maintenance of services

### Q8: What purposes does an API Gateway serve ?
Serve as a single point of entry for services, managing security, throughput limitation and request distribution
